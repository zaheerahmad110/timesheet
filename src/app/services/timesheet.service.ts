import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TimeSheetService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getTimeSheet(Id) {
        return this.http.get(this.baseapi + '/taskhours/' + Id);
    }
    saveTimeSheet(timeSheet: any[]) {
        return this.http.post(this.baseapi+'/taskhours', timeSheet);
    }
}
