import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TaskComponent } from './task/task.component';
import { TaskService } from './services/task.service';
import { FormsModule } from '@angular/forms';
import { EmployeeTimesheetComponent } from './employee.timesheet/employee.timesheet.component';

import { CeilPipe } from './pipe/ceil.pipe';
import { TimeSheetService } from './services/timesheet.service';


@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TaskComponent,
    EmployeeTimesheetComponent,
    CeilPipe
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: EmployeeListComponent, pathMatch: 'full' },
      { path: 'employees', component: EmployeeListComponent },
      { path: 'timesheet/:id', component: EmployeeTimesheetComponent }
    ])
  ],
  providers: [
    EmployeeService,
    TaskService,
    TimeSheetService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {


}

