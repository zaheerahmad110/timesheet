import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-employee-list',
    templateUrl: 'employee.component.html',
    styleUrls: ['./employee.component.scss']
})

export class EmployeeListComponent implements OnInit {
    employees: any;
    constructor(private employeeService: EmployeeService, private router: Router) { }

    ngOnInit() {
        this.employeeService.getEmployees().subscribe(data => {
            this.employees = data;
        });
    }
    GetEmployeeHours(taskHours): number {
        let hours = 0;
        taskHours.forEach(taskHour => {
            hours += taskHour.monday;
            hours += taskHour.tuesday;
            hours += taskHour.wednesday;
            hours += taskHour.thursday;
            hours += taskHour.friday;
            hours += taskHour.saturday;
            hours += taskHour.sunday;
        });
        return hours;
    }
    navigateToList(id) {
        this.router.navigate(['/timesheet/' + id]);
        console.log(id);
    }
}
