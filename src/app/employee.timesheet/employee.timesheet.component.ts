import { Component, OnInit } from '@angular/core';
import { TimeSheetService } from '../services/timesheet.service';
import { EmployeeService } from '../services/employee.service';
import { TaskService } from '../services/task.service';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-employee.timesheet',
  templateUrl: './employee.timesheet.component.html',
  styleUrls: ['./employee.timesheet.component.scss']
})
export class EmployeeTimesheetComponent implements OnInit {
  employeeTimeSheet: any;
  constructor(private timeSheetService: TimeSheetService,
    private employeeService: EmployeeService, private taskService: TaskService, private route: ActivatedRoute
    , private router: Router
    ) { }


     tasks: any = [];
     timeSheet: any = [];
     taskTimeSheet: any = {};
     employeesHours: {};
     employees: any = [];
     editIndex = -1;
     employeeId: string;
     public cellName:string='';
     public footer:any = {};

     ngOnInit() {
       this.employeeId = this.route.snapshot.paramMap.get('id');
       console.log(this.employeeId);
       this.route.paramMap.subscribe(params => {
        this.employeeId = params.get("id");
        this.loadData();
       })

    }
   
    loadData()
    {
      this.timeSheetService.getTimeSheet(this.employeeId).subscribe(data => {
        this.timeSheet = data;
        this.calculateFooter();
      });
        this.taskService.getTasks().subscribe(data => {
          this.tasks = data;

      });
        this.employeeService.getDDEmployees().subscribe(data => {
          this.employees = data;
      });
    }
    addRow(){
      console.log(this.tasks[0]);
      this.timeSheet.push({id:0,employeeId:parseInt(this.employeeId),taskId:this.tasks[0].id,taskName:this.tasks[0].name,sunday:0,monday:0,tuesday:0,wednesday:0,thursday:0,friday:0,saturday:0});
    }
    saveTimeSheet(){
      this.timeSheetService.saveTimeSheet(this.timeSheet).subscribe((data:any) => {
        if(data.status == 'success'){
          alert('Records are updated');
        }
      });
    }
    editTask(taskTimeSheet: any, index: number) {
        this.taskTimeSheet = taskTimeSheet;
        this.editIndex = index;
    }
    editTaskSubmit(index: number) {

        this.tasks[index] = this.taskTimeSheet;
        var _taskName = this.tasks.filter((t:any)=>t.id == this.taskTimeSheet.taskId)[0];
        if(_taskName != undefined){
          this.tasks[index]['taskName'] =_taskName.name;
        }
        console.log(this.tasks[index]);
        this.editIndex = -1;
        this.calculateFooter();
    }
    onSelectEmployee() {
       console.log(this.employeeId);
      this.router.navigate(['/timesheet/' + this.employeeId]);

    }

  SaveTimeSheet()
  {
      if(this.tasks.length > 0){
        this.timeSheetService.saveTimeSheet(this.timeSheet);
      }
  }
  cellClick(item:any,index:number,cellName:string){
    this.tasks[index][cellName] = this.taskTimeSheet[cellName];
   
    
    this.editIndex = index;
    this.cellName = cellName;
  }
  cellBlur(value:any,index:number,cellName:string){
    if(cellName == undefined || cellName == ''){
        if(cellName != 'taskName'){
          this.taskTimeSheet[cellName] = 0;
        }
    }
    else{
      this.taskTimeSheet[cellName] = this.tasks[index][cellName];
    }
    this.editIndex = -1;
    this.cellName = '';
    this.calculateFooter();
  }
  calculateFooter(){
    this.footer.TotalSunday = this.sum('sunday');
    this.footer.TotalMonday = this.sum('monday');
    this.footer.TotalTuesday = this.sum('tuesday');
    this.footer.TotalWednesday = this.sum('wednesday');
    this.footer.TotalThursday = this.sum('thursday');
    this.footer.TotalFriday = this.sum('friday');
    this.footer.TotalSaturday = this.sum('saturday');
  }
  sum(prop) {
    var total = 0
    for ( var i = 0, _len = this.timeSheet.length; i < _len; i++ ) {
        total += this.timeSheet[i][prop]
    }
    return total;
}


}
